#!/bin/bash

mkdir -p ${PREFIX}/share/snmp/mibs
cp -Rf iana ${PREFIX}/share/snmp/mibs

mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/snmp-mibs-iana_activate.sh
IANAMIBDIR="${PREFIX}/share/snmp/mibs/iana"
[  -z "\$MIBDIRS" ] && export MIBDIRS="\$IANAMIBDIR" || export MIBDIRS="\$MIBDIRS:\$IANAMIBDIR"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/snmp-mibs-iana_deactivate.sh
IANAMIBDIR="${PREFIX}/share/snmp/mibs/iana"
export MIBDIRS=\$(echo -n \$MIBDIRS | tr ":" "\n" | grep -xv "\$IANAMIBDIR" | tr "\n" ":" | rev | cut -c 2- | rev)
EOF
