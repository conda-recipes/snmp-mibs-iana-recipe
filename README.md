# snmp-mibs-iana conda recipe

Home: "https://www.iana.org/"

Recipe license: BSD 3-Clause

Summary: SNMP MIBs maintained by IANA
